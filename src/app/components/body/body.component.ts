import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-body',
  templateUrl: './body.component.html',
  styleUrls: ['./body.component.css']
})
export class BodyComponent implements OnInit {
  frase:any = {
    mensaje: 'La sabiduría viene de la experiencia. La experiencia es, a menudo, el resultado de la falta de sabiduría',
    autor: 'Terry Pratchett',
  };
  mostrar:boolean=true;
  valorNumerico:number = 1;
  personas:string[]= ["pedro", "Carlos", "maria"];
  constructor() { }

  ngOnInit(): void {
  }

}
